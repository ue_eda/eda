package aula_05.code3;

public class TestLinkedList {
  public static void main(String args[]) {
	//Exercicio 01
	  
    LinkedList<Integer> l = new LinkedList<Integer>();
    LinkedList<Character> l2 = new LinkedList<Character>();
    
    for(int i = 8 ; i > 0 ; i--)
    LibLinkedList.addFirst(l, new Integer(i));
    System.out.println("list size: " + LibLinkedList.size(l));
    
    //LibLinkedList.add(l, new Integer(55), 5);
    LibLinkedList.add(l, 55, 5);
    
    //LibLinkedList.add(l, new Integer(22), 9);
    LibLinkedList.add(l, 22, 9);
    
    //LibLinkedList.addLast(l, new Integer(33));
    LibLinkedList.addLast(l, 33);
    
    System.out.println("list size: " + LibLinkedList.size(l));
    System.out.println(LibLinkedList.toString(l));
    LibLinkedList.removeFirst(l); 
    LibLinkedList.remove(l, 3);
    System.out.println("indexOf(6): " + LibLinkedList.indexOf(l, 6));
    System.out.println(LibLinkedList.toString(l)+"\n");
    
    LibLinkedList.addFirst(l2, new Character('a'));
    LibLinkedList.add(l2, 'e', 1);
    LibLinkedList.add(l2, 'i', 2);
    LibLinkedList.add(l2, 'o', 3);
    LibLinkedList.addLast(l2, 'u');
    System.out.println("list size: " + LibLinkedList.size(l2));
    System.out.println(LibLinkedList.toString(l2)+"\n");
    
    //EXERCICIO 03
    
    //ED145 - Concatenação de Listas
    //Primeira Lista
    LinkedList<Integer> a = new LinkedList<Integer>();
    for(int i = 5 ; i > 0 ; i--)
        LibLinkedList.addFirst(a, new Integer(i));
    System.out.println("Primeira Lista: " + LibLinkedList.toString(a));
    
    //Segunda Lista
    LinkedList<Integer> b = new LinkedList<Integer>();
	LibLinkedList.addFirst(b, 6);
	LibLinkedList.add(b, 7, 1);
    LibLinkedList.addLast(b, 8);
    System.out.println("Segunda Lista: " + LibLinkedList.toString(b));
    
    //Listas Concatenadas
     LibLinkedList.concatenate(a, b);
     
    
    //ED146 - Duplicando Elementos
    
    //ED150 - Filtrando uma lista
    
  }
}
