package aula_05.ex02;

import aula_05.code3.LibLinkedList;

public class LibAccount {
	public static void deposit(Account a, double m) {
		a.balance += m;
		LibLinkedList.addLast(a.movements, m);
	    }

	    public static void withdraw(Account a, double m) {
		a.balance -= m;
		LibLinkedList.addLast(a.movements, -m);
	    }
	    
	    public static void getMovements(Account a) {
	    	 System.out.printf("Saldo: " + "%.2f\n", a.balance);
	    	 System.out.println("Numero de Movimentos: " + LibLinkedList.size(a.movements));
	    	 for(int i = 0 ; i > 1 ; i--)
	    	 System.out.println(a.movements);
	    }
}
