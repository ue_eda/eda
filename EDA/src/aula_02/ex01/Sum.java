package aula_02.ex01;

//Importa a classe Scanner, que ser� usada para leitura de dados
import java.util.Scanner;
public class Sum {
public static void main(String[] args) {
//Cria um objecto Scanner para ler da entrada padr�o ("standard input")
Scanner stdin = new Scanner(System.in);
//Chama o m�todo nextInt() para ir buscar o pr�ximo inteiro
double a = stdin.nextDouble();
double b = stdin.nextDouble();
//Imprime a soma dos dois n�meros
System.out.println(a+b);
}
}
