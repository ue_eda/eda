package aula_02.ex03;

import java.util.Scanner;
import java.util.stream.IntStream;
public class ReadNumbers {
public static void main(String[] args) {
Scanner stdin = new Scanner(System.in);
// Cria um novo array com espa�o para 10 inteiros
int v[] = new int[11];	
int soma = 0;
int media = 0;
int amp = 0;
 // Ler a quantidade de numeros que se seguem
int n = stdin.nextInt();
// Ler os numeros a partir do stdin
for (int i=0; i<n; i++)
v[i] = stdin.nextInt();
// Escrever os numeros no stdout
for (int i=0; i<n; i++) 
System.out.println("v[" + i + "] = " + v[i]);
//Soma
for (int i=0; i<n; i++)
soma += v[i];
System.out.println("Soma = " + soma);
//Media
media += soma/n;
System.out.println("Media = " + media);
}
}