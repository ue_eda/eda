package aula_01.ex02;

//Uma classe simples para representar um aluno
class Aluno {
//Atributos da classe (vari�veis para conter informa��o)
String nome;
int numero;
//Construtor "padr�o"
Aluno() {
nome = "indefinido";
numero = -1;
}

Aluno(String n, int mec) {
	 nome = n;
	 numero = mec;
	}

}

//Classe principal contendo o main para testar a classe Aluno
public class TestAluno {
public static void main(String[] args) {
Aluno a = new Aluno();
Aluno b = new Aluno();
Aluno c = new Aluno();
c.nome = "modificado";
Aluno d = c;
Aluno e = new Aluno();
Aluno f = new Aluno("Manuel", 201506234);

//Escrever o conte�do do aluno "a"
//Note que o operador + � usado para concatenar duas strings
System.out.println("Aluno a | numero: " + a.numero + " | nome: " + a.nome);
System.out.println(a);
System.out.println("Aluno b | numero: " + b.numero + " | nome: " + b.nome);
System.out.println("Aluno c | numero: " + c.numero + " | nome: " + c.nome);
System.out.println("Aluno d | numero: " + d.numero + " | nome: " + d.nome);
System.out.println("Aluno e | numero: " + e.numero + " | nome: " + e.nome);
System.out.println("Aluno f | numero: " + f.numero + " | nome: " + f.nome);
}
}